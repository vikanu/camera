# Overview

akses ipcamera menggunakan lib video surveillance (ispy)

#### Packages
* namespace ispyaplication
* nVLC

```csharp
using iSpyApplication.Controls;
using iSpyApplication.Sources.Audio;
using iSpyApplication.Utilities;
using Implementation;
```

#### Installing
untuk menjalankan program, install file `setup.msi` atau `setup.exe` yang terletak di folder Installer.

#### Panduan Program

Sebelum menjalankan program, pastikan terlebih dahulu ip komputer dan ipcamera berada dalam satu jaringan.

1. Buka program yang telah diinstall. 

2. Klik menu add --> pilih source vlc untuk stream dengan url. contoh url : https://36.81.202.158/camera/baruna.m3u8 --> tekan tombol `ok`.

    ![Add](images/videosource.PNG)

2. Pada Tab `camera` isikan nama camera . contoh : camera baruna

    ![Add](images/vscamera.PNG)

3. Pada Tab `images` isikan name tag dan selanjutnya tekan tombol `finish`.

    ![Add](images/vsimages.PNG).

4. Berikut ini hasil streaming dari url camera baruna.

    ![Add](images/baruna.PNG).

5. Untuk menyimpan gambar hassil streaming, dapat dilakukan dengan cara pada panell viewer camera baruna klik kanan mouse pilih menu `take photo`

    ![takephto](images/takephoto.PNG).

6. Untuk record video hasil streaming, dapat dilakukan dengan cara pada panell viewer camera baruna klik kanan mouse pilih menu `start recording`

    ![recordstart](images/recordon.PNG).

7. Untuk stop video recording, dapat dilakukan dengan cara pada panell viewer camera baruna klik kanan mouse pilih menu `stop recording`

    ![recordstop](images/recordoff.PNG).



#### Alur Program
Berikut ini merupakan breakdown dari program.

![alur](Documentation/alur.PNG).

##### ket :

1. [Form Utama](https://bitbucket.org/vikanu/camera/src/master/Documentation/mainform.md)

2. [Add Camera](https://bitbucket.org/vikanu/camera/src/master/Documentation/addcamera.md)

3. [Remove Camera](https://bitbucket.org/vikanu/ipcamera/src/master/Documentation/removecamera.md)


#### Case : Akses camera menggunakan URL camera 
1.	http://36.81.202.158/camera/baruna.m3u8
2.	http://36.81.202.158/camera/iskandar_muda.m3u8
3.	http://36.81.202.158/camera/muning.m3u8
4.	http://36.81.202.158/camera/kawi.m3u8

#### Hardware Uji Fungsi:
1.	Dome Camera (PTZ Camera) - HIK
2.	Network Bullet Camera � HIK

#### Softaware Uji Fungsi:

1.	Windows 7 ultimate (minimal)
2.	Visual Studio 2015 (minimal)

#### Hasil  :

+ Program dapat mengakses beberapa kamera (dalam percobaan menggunakan 4 ipcamera seperti contoh diatas)

![multicam](images/tesmulticam.PNG).

+ Terdapat jeda (putus-putus) pada saat streaming, hal ini bisa terjadi karena pengaruh jaringan internet.

+ Hasil streaming (gambar) warna, antara 4 URL tersebut ada perbedaan terdapat kualitas gambar yang kurang tajam dan pecah.

#### Case : Akses camera menggunakan ip address, username dan password camera HIK 

#### Hasil  :

+ Program dapat mengakses beberapa kamera (dalam percobaan menggunakan 2 kamera)

+ Disini terdapat perbedaan kualitas gambar(hasil streaming) apabila akses kamera menggunakan ipaddress, username,  password dan menggunakan local jaringan maka hasil atau kulalitas streaming lebih jelas dan tidak putus.


#### Installer
- running file .exe terletak pada foldel bin\x86\Debug\ipcamaera.exe