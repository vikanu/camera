﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSpyApplication
{
    public partial class FrmSplash : Form
    {
        int timerLoad = 0;
        bool silentstartup = false;
        string command = "";

        public FrmSplash()
        {
            InitializeComponent();
        }

        private void FrmSplash_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            timer1.Interval = 50;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timerLoad += 1;
            if (timerLoad >= 100)
            {
                timer1.Enabled = false;
                timer1.Stop();
                this.Hide();

                MainForm mf = new MainForm(silentstartup, command);
                mf.Show();
            }

            pgProses.Value = timerLoad;
        }
    }
}
