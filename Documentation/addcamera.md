Mainform.cs menu `add`

```csharp
//3---> Tag tab dari sourcevideo
AddCamera(3);
```

memanggil function pada form Mainform_Configuration.cs

add `NewCameraWindow(3)`

```csharp
private void AddCamera(int videoSourceIndex, bool startWizard = false)
        {
            //dengan result videosourceIndex 3
            CameraWindow cw = NewCameraWindow(videoSourceIndex);
            TopMost = false;
            var ac = new AddCamera { CameraControl = cw, StartWizard = startWizard, IsNew = true, MainClass = this };
            ac.ShowDialog(this);
            if (ac.DialogResult == DialogResult.OK)
            {
                UnlockLayout();
                SetNewStartPosition();
                if (cw.VolumeControl != null && !cw.VolumeControl.IsEnabled)
                    cw.VolumeControl.Enable();
                NeedsSync = true;
                SaveObjects();
            }
```

memanggil  function `private void SetupVideoSource()` mengarah pada class videosource.cs.
sourceIndex untuk vlc : 5 

```csharp
case 5:
                    //cek program vlc sudah terinstall apa belum
                    if (!VlcHelper.VlcInstalled)
                    {
                        MessageBox.Show(LocRm.GetString("DownloadVLC"), LocRm.GetString("Note"));
                        return;
                    }
                    if (!_vlcStreamSizeSet)
                    {
                        CameraControl.Camobject.settings.vlcWidth = 640;
                        CameraControl.Camobject.settings.vlcHeight = 480;
                    }
                    //respon : http://36.81.202.158/camera/baruna.m3u8
                    url = cmbVLCURL.Text.Trim();
                    if (url == string.Empty)
                    {
                        MessageBox.Show(LocRm.GetString("Validate_SelectCamera"), LocRm.GetString("Note"));
                        return;
                    }
                    VideoSourceString = url;
                    CameraControl.Camobject.settings.vlcargs = txtVLCArgs.Text.Trim();
```

function `void Enable` pada class CameraWindow.cs 

```csharp
switch (Camobject.settings.sourceindex)
                {
                    //Tag 5 merupakan Tag dari tab VLC
                   case 5:
                        // memanggil function pada class VLCStream
                        var vlcSource = new VlcStream(this);
                        OpenVideoSource(vlcSource, true);
                        break;
```
Tag 5 merupakan Tag dari Tab sourcevideo vlc.

`OpenVideoSource(vlcSource, true)` mengarah pada function `void OpenVideoSource` pada class CameraWindow.cs

```csharp
var source = cw.Camera.VideoSource;
                //source VLCStream
                Camera = new Camera(source);
```

memanggil `Start()` pada class VLCStream

```csharp
if (!VlcHelper.VlcInstalled)
                return;

            if (IsRunning) return;

            _res = ReasonToFinishPlaying.DeviceLost;

            // create and start new thread

            _thread = new Thread(WorkerThread) { Name = Source, IsBackground = true };
            _thread.SetApartmentState(ApartmentState.MTA);
            _thread.Start();
```



