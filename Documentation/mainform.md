Program diarahkan pada splashscreean

pada program.cs

```csharp
var mf = new FrmSplash();
```

memanggil form `FrmSplash` dan event untuk load MainForm.cs(halaman utama) diletakkan pada timer

```csharp
private void timer1_Tick(object sender, EventArgs e)
        {
            timerLoad += 1;
            if (timerLoad >= 100)
            {
                timer1.Enabled = false;
                timer1.Stop();
                this.Hide();

                MainForm mf = new MainForm(silentstartup, command);
                mf.Show();
            }

            pgProses.Value = timerLoad;
        }
```

