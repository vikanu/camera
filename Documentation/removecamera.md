Mainform.cs menu `remove`

```csharp
private void DeleteToolStripMenuItemClick(object sender, EventArgs e)
        {
            var window = ContextTarget as CameraWindow;
            if (window != null)
            {
                RemoveCamera(window, true);
                return;
            }
```

memanggil function `RemoveCamera()` pada form Mainform_Configuration.cs

```csharp
public void RemoveCamera(CameraWindow cameraControl, bool confirm)
        {
            if (confirm &&
                MessageBox.Show(LocRm.GetString("Delete")+":" +cameraControl.ObjectName, LocRm.GetString("Confirm"), MessageBoxButtons.OKCancel,
                                MessageBoxIcon.Warning) == DialogResult.Cancel)
                return;
            //tampilkan dialog box
            var dr = DialogResult.No;
            if (confirm)
            {
                dr = MessageBox.Show(LocRm.GetString("DeleteAllAssociatedMedia"), LocRm.GetString("Confirm"),
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Question);
            }
```
