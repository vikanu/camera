using iSpyApplication.Controls;

namespace iSpyApplication
{
    partial class VideoSource
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VideoSource));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.ofd = new System.Windows.Forms.OpenFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button4 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.videoResolutionsCombo = new System.Windows.Forms.ComboBox();
            this.devicesCombo = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.videoInputsCombo = new System.Windows.Forms.ComboBox();
            this.snapshotResolutionsCombo = new System.Windows.Forms.ComboBox();
            this.chkAutoImageSettings = new System.Windows.Forms.CheckBox();
            this.snapshotsLabel = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.rdoCaptureVideo = new System.Windows.Forms.RadioButton();
            this.rdoCaptureSnapshots = new System.Windows.Forms.RadioButton();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.label40 = new System.Windows.Forms.Label();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.pnlVLC = new System.Windows.Forms.Panel();
            this.tlpVLC = new System.Windows.Forms.TableLayoutPanel();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmbVLCURL = new System.Windows.Forms.ComboBox();
            this.button6 = new System.Windows.Forms.Button();
            this.btnGetStreamSize = new System.Windows.Forms.Button();
            this.linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.label16 = new System.Windows.Forms.Label();
            this.tcSource = new System.Windows.Forms.TabControl();
            this.txtVLCArgs = new iSpyApplication.Controls.ClipboardTextBox();
            this.onvifWizard1 = new iSpyApplication.Controls.ONVIFWizard();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.flowLayoutPanel12.SuspendLayout();
            this.pnlVLC.SuspendLayout();
            this.tlpVLC.SuspendLayout();
            this.flowLayoutPanel11.SuspendLayout();
            this.tcSource.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button1.Location = new System.Drawing.Point(347, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(32, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1Click);
            // 
            // button2
            // 
            this.button2.AutoSize = true;
            this.button2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button2.Location = new System.Drawing.Point(291, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(50, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Controls.Add(this.button2);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(351, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.flowLayoutPanel1.Size = new System.Drawing.Size(382, 35);
            this.flowLayoutPanel1.TabIndex = 60;
            // 
            // button4
            // 
            this.button4.AutoSize = true;
            this.button4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button4.Location = new System.Drawing.Point(4, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(142, 23);
            this.button4.TabIndex = 60;
            this.button4.Text = "Use the IP Camera Wizard";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.flowLayoutPanel1);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(3, 317);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(733, 35);
            this.panel2.TabIndex = 61;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.onvifWizard1);
            this.tabPage10.Controls.Add(this.splitter1);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(725, 288);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "Ipcamera";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 288);
            this.splitter1.TabIndex = 58;
            this.splitter1.TabStop = false;
            // 
            // tabPage4
            // 
            this.tabPage4.AutoScroll = true;
            this.tabPage4.Controls.Add(this.tableLayoutPanel4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(6);
            this.tabPage4.Size = new System.Drawing.Size(725, 288);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Webcam";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.93408F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 81.06592F));
            this.tableLayoutPanel4.Controls.Add(this.label39, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label38, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.videoResolutionsCombo, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.devicesCombo, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label37, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.videoInputsCombo, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.snapshotResolutionsCombo, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.chkAutoImageSettings, 1, 5);
            this.tableLayoutPanel4.Controls.Add(this.snapshotsLabel, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.label35, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.flowLayoutPanel5, 1, 2);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(6, 6);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 6;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(713, 229);
            this.tableLayoutPanel4.TabIndex = 22;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 6);
            this.label39.Margin = new System.Windows.Forms.Padding(6);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(72, 13);
            this.label39.TabIndex = 11;
            this.label39.Text = "Video device:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(6, 99);
            this.label38.Margin = new System.Windows.Forms.Padding(6);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(83, 13);
            this.label38.TabIndex = 12;
            this.label38.Text = "Video resoluton:";
            // 
            // videoResolutionsCombo
            // 
            this.videoResolutionsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.videoResolutionsCombo.FormattingEnabled = true;
            this.videoResolutionsCombo.Location = new System.Drawing.Point(137, 96);
            this.videoResolutionsCombo.Name = "videoResolutionsCombo";
            this.videoResolutionsCombo.Size = new System.Drawing.Size(127, 21);
            this.videoResolutionsCombo.TabIndex = 13;
            this.videoResolutionsCombo.SelectedIndexChanged += new System.EventHandler(this.videoResolutionsCombo_SelectedIndexChanged);
            // 
            // devicesCombo
            // 
            this.devicesCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.devicesCombo.FormattingEnabled = true;
            this.devicesCombo.Location = new System.Drawing.Point(137, 3);
            this.devicesCombo.Name = "devicesCombo";
            this.devicesCombo.Size = new System.Drawing.Size(127, 21);
            this.devicesCombo.TabIndex = 9;
            this.devicesCombo.SelectedIndexChanged += new System.EventHandler(this.devicesCombo_SelectedIndexChanged_1);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(6, 37);
            this.label37.Margin = new System.Windows.Forms.Padding(6);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(63, 13);
            this.label37.TabIndex = 16;
            this.label37.Text = "Video input:";
            // 
            // videoInputsCombo
            // 
            this.videoInputsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.videoInputsCombo.FormattingEnabled = true;
            this.videoInputsCombo.Location = new System.Drawing.Point(137, 34);
            this.videoInputsCombo.Name = "videoInputsCombo";
            this.videoInputsCombo.Size = new System.Drawing.Size(127, 21);
            this.videoInputsCombo.TabIndex = 17;
            this.videoInputsCombo.SelectedIndexChanged += new System.EventHandler(this.videoInputsCombo_SelectedIndexChanged);
            // 
            // snapshotResolutionsCombo
            // 
            this.snapshotResolutionsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.snapshotResolutionsCombo.FormattingEnabled = true;
            this.snapshotResolutionsCombo.Location = new System.Drawing.Point(137, 127);
            this.snapshotResolutionsCombo.Name = "snapshotResolutionsCombo";
            this.snapshotResolutionsCombo.Size = new System.Drawing.Size(127, 21);
            this.snapshotResolutionsCombo.TabIndex = 14;
            this.snapshotResolutionsCombo.SelectedIndexChanged += new System.EventHandler(this.snapshotResolutionsCombo_SelectedIndexChanged);
            // 
            // chkAutoImageSettings
            // 
            this.chkAutoImageSettings.AutoSize = true;
            this.chkAutoImageSettings.Location = new System.Drawing.Point(140, 161);
            this.chkAutoImageSettings.Margin = new System.Windows.Forms.Padding(6);
            this.chkAutoImageSettings.Name = "chkAutoImageSettings";
            this.chkAutoImageSettings.Size = new System.Drawing.Size(146, 17);
            this.chkAutoImageSettings.TabIndex = 18;
            this.chkAutoImageSettings.Text = "Automatic Image Settings";
            this.chkAutoImageSettings.UseVisualStyleBackColor = true;
            // 
            // snapshotsLabel
            // 
            this.snapshotsLabel.AutoSize = true;
            this.snapshotsLabel.Location = new System.Drawing.Point(6, 130);
            this.snapshotsLabel.Margin = new System.Windows.Forms.Padding(6);
            this.snapshotsLabel.Name = "snapshotsLabel";
            this.snapshotsLabel.Size = new System.Drawing.Size(101, 13);
            this.snapshotsLabel.TabIndex = 15;
            this.snapshotsLabel.Text = "Snapshot resoluton:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 68);
            this.label35.Margin = new System.Windows.Forms.Padding(6);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(76, 13);
            this.label35.TabIndex = 19;
            this.label35.Text = "Capture mode:";
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.rdoCaptureVideo);
            this.flowLayoutPanel5.Controls.Add(this.rdoCaptureSnapshots);
            this.flowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(134, 62);
            this.flowLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(579, 31);
            this.flowLayoutPanel5.TabIndex = 20;
            // 
            // rdoCaptureVideo
            // 
            this.rdoCaptureVideo.AutoSize = true;
            this.rdoCaptureVideo.Location = new System.Drawing.Point(6, 6);
            this.rdoCaptureVideo.Margin = new System.Windows.Forms.Padding(6);
            this.rdoCaptureVideo.Name = "rdoCaptureVideo";
            this.rdoCaptureVideo.Size = new System.Drawing.Size(52, 17);
            this.rdoCaptureVideo.TabIndex = 0;
            this.rdoCaptureVideo.TabStop = true;
            this.rdoCaptureVideo.Text = "Video";
            this.rdoCaptureVideo.UseVisualStyleBackColor = true;
            // 
            // rdoCaptureSnapshots
            // 
            this.rdoCaptureSnapshots.AutoSize = true;
            this.rdoCaptureSnapshots.Location = new System.Drawing.Point(70, 6);
            this.rdoCaptureSnapshots.Margin = new System.Windows.Forms.Padding(6);
            this.rdoCaptureSnapshots.Name = "rdoCaptureSnapshots";
            this.rdoCaptureSnapshots.Size = new System.Drawing.Size(75, 17);
            this.rdoCaptureSnapshots.TabIndex = 1;
            this.rdoCaptureSnapshots.TabStop = true;
            this.rdoCaptureSnapshots.Text = "Snapshots";
            this.rdoCaptureSnapshots.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.AutoScroll = true;
            this.tabPage6.Controls.Add(this.flowLayoutPanel12);
            this.tabPage6.Controls.Add(this.pnlVLC);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(6);
            this.tabPage6.Size = new System.Drawing.Size(725, 288);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "VLC";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.Controls.Add(this.label40);
            this.flowLayoutPanel12.Controls.Add(this.linkLabel3);
            this.flowLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel12.Location = new System.Drawing.Point(6, 233);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(713, 25);
            this.flowLayoutPanel12.TabIndex = 59;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.ForeColor = System.Drawing.Color.Red;
            this.label40.Location = new System.Drawing.Point(3, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(0, 13);
            this.label40.TabIndex = 58;
            // 
            // linkLabel3
            // 
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.Location = new System.Drawing.Point(9, 0);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(78, 13);
            this.linkLabel3.TabIndex = 57;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "Download VLC";
            this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel3LinkClicked);
            // 
            // pnlVLC
            // 
            this.pnlVLC.AutoSize = true;
            this.pnlVLC.Controls.Add(this.tlpVLC);
            this.pnlVLC.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlVLC.Location = new System.Drawing.Point(6, 6);
            this.pnlVLC.Name = "pnlVLC";
            this.pnlVLC.Size = new System.Drawing.Size(713, 227);
            this.pnlVLC.TabIndex = 56;
            this.pnlVLC.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlVLC_Paint);
            // 
            // tlpVLC
            // 
            this.tlpVLC.AutoSize = true;
            this.tlpVLC.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tlpVLC.ColumnCount = 2;
            this.tlpVLC.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpVLC.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpVLC.Controls.Add(this.label21, 0, 0);
            this.tlpVLC.Controls.Add(this.label19, 1, 1);
            this.tlpVLC.Controls.Add(this.txtVLCArgs, 1, 2);
            this.tlpVLC.Controls.Add(this.label18, 0, 2);
            this.tlpVLC.Controls.Add(this.flowLayoutPanel11, 1, 0);
            this.tlpVLC.Controls.Add(this.label16, 1, 3);
            this.tlpVLC.Dock = System.Windows.Forms.DockStyle.Top;
            this.tlpVLC.Location = new System.Drawing.Point(0, 0);
            this.tlpVLC.Name = "tlpVLC";
            this.tlpVLC.RowCount = 5;
            this.tlpVLC.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tlpVLC.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpVLC.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tlpVLC.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tlpVLC.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tlpVLC.Size = new System.Drawing.Size(713, 227);
            this.tlpVLC.TabIndex = 58;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 8);
            this.label21.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(52, 13);
            this.label21.TabIndex = 50;
            this.label21.Text = "VLC URL";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(66, 31);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(297, 13);
            this.label19.TabIndex = 53;
            this.label19.Text = "eg: http://username:password@192.168.1.4/videostream.asf";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 64);
            this.label18.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(57, 13);
            this.label18.TabIndex = 52;
            this.label18.Text = "Arguments";
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.Controls.Add(this.cmbVLCURL);
            this.flowLayoutPanel11.Controls.Add(this.button6);
            this.flowLayoutPanel11.Controls.Add(this.btnGetStreamSize);
            this.flowLayoutPanel11.Controls.Add(this.linkLabel5);
            this.flowLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel11.Location = new System.Drawing.Point(63, 0);
            this.flowLayoutPanel11.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(650, 31);
            this.flowLayoutPanel11.TabIndex = 60;
            // 
            // cmbVLCURL
            // 
            this.cmbVLCURL.FormattingEnabled = true;
            this.cmbVLCURL.Location = new System.Drawing.Point(3, 3);
            this.cmbVLCURL.Name = "cmbVLCURL";
            this.cmbVLCURL.Size = new System.Drawing.Size(297, 21);
            this.cmbVLCURL.TabIndex = 49;
            // 
            // button6
            // 
            this.button6.AutoSize = true;
            this.button6.Location = new System.Drawing.Point(306, 3);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(30, 27);
            this.button6.TabIndex = 60;
            this.button6.Text = "...";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // btnGetStreamSize
            // 
            this.btnGetStreamSize.Location = new System.Drawing.Point(342, 3);
            this.btnGetStreamSize.Name = "btnGetStreamSize";
            this.btnGetStreamSize.Size = new System.Drawing.Size(104, 23);
            this.btnGetStreamSize.TabIndex = 58;
            this.btnGetStreamSize.Text = "Get Video Size";
            this.btnGetStreamSize.UseVisualStyleBackColor = true;
            this.btnGetStreamSize.Click += new System.EventHandler(this.Button4Click);
            // 
            // linkLabel5
            // 
            this.linkLabel5.AutoSize = true;
            this.linkLabel5.Location = new System.Drawing.Point(455, 6);
            this.linkLabel5.Margin = new System.Windows.Forms.Padding(6);
            this.linkLabel5.Name = "linkLabel5";
            this.linkLabel5.Size = new System.Drawing.Size(29, 13);
            this.linkLabel5.TabIndex = 59;
            this.linkLabel5.TabStop = true;
            this.linkLabel5.Text = "Help";
            this.linkLabel5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel5_LinkClicked);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(66, 146);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(311, 13);
            this.label16.TabIndex = 62;
            this.label16.Text = "Add \"--ffmpeg-hw\" to enable H264 GPU decoding (experimental)";
            // 
            // tcSource
            // 
            this.tcSource.Controls.Add(this.tabPage6);
            this.tcSource.Controls.Add(this.tabPage4);
            this.tcSource.Controls.Add(this.tabPage10);
            this.tcSource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcSource.Location = new System.Drawing.Point(3, 3);
            this.tcSource.Name = "tcSource";
            this.tcSource.SelectedIndex = 0;
            this.tcSource.Size = new System.Drawing.Size(733, 314);
            this.tcSource.TabIndex = 57;
            this.tcSource.SelectedIndexChanged += new System.EventHandler(this.tcSource_SelectedIndexChanged);
            // 
            // txtVLCArgs
            // 
            this.txtVLCArgs.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtVLCArgs.Location = new System.Drawing.Point(66, 59);
            this.txtVLCArgs.Multiline = true;
            this.txtVLCArgs.Name = "txtVLCArgs";
            this.txtVLCArgs.Size = new System.Drawing.Size(294, 84);
            this.txtVLCArgs.TabIndex = 61;
            this.txtVLCArgs.PastedText += new iSpyApplication.Controls.ClipboardTextBox.ClipboardEventHandler(this.txtVLCArgs_PastedText);
            // 
            // onvifWizard1
            // 
            this.onvifWizard1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.onvifWizard1.Location = new System.Drawing.Point(3, 0);
            this.onvifWizard1.Name = "onvifWizard1";
            this.onvifWizard1.Size = new System.Drawing.Size(722, 288);
            this.onvifWizard1.TabIndex = 59;
            this.onvifWizard1.Load += new System.EventHandler(this.onvifWizard1_Load);
            // 
            // VideoSource
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(739, 355);
            this.Controls.Add(this.tcSource);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "VideoSource";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Video Source";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VideoSource_FormClosing);
            this.Load += new System.EventHandler(this.VideoSourceLoad);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage10.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.flowLayoutPanel12.ResumeLayout(false);
            this.flowLayoutPanel12.PerformLayout();
            this.pnlVLC.ResumeLayout(false);
            this.pnlVLC.PerformLayout();
            this.tlpVLC.ResumeLayout(false);
            this.tlpVLC.PerformLayout();
            this.flowLayoutPanel11.ResumeLayout(false);
            this.flowLayoutPanel11.PerformLayout();
            this.tcSource.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.OpenFileDialog ofd;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabPage tabPage10;
        private ONVIFWizard onvifWizard1;
        private System.Windows.Forms.Splitter splitter1;
        public System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox videoResolutionsCombo;
        private System.Windows.Forms.ComboBox devicesCombo;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ComboBox videoInputsCombo;
        private System.Windows.Forms.ComboBox snapshotResolutionsCombo;
        private System.Windows.Forms.CheckBox chkAutoImageSettings;
        private System.Windows.Forms.Label snapshotsLabel;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.RadioButton rdoCaptureVideo;
        private System.Windows.Forms.RadioButton rdoCaptureSnapshots;
        public System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.Panel pnlVLC;
        private System.Windows.Forms.TableLayoutPanel tlpVLC;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label19;
        private ClipboardTextBox txtVLCArgs;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.ComboBox cmbVLCURL;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button btnGetStreamSize;
        private System.Windows.Forms.LinkLabel linkLabel5;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.TabControl tcSource;
    }
}